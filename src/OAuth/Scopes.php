<?php


namespace Wukongke\HiHealth\OAuth;


abstract class Scopes
{
    // hihealth
    const PROFILE = 'https://www.huawei.com/health/profile';
    const PROFILE_READONLY = 'https://www.huawei.com/health/profile.readonly';
    const SPORT = 'https://www.huawei.com/health/sport';
    const SPORT_READONLY = 'https://www.huawei.com/health/sport.readonly';
    const HEALTH_WGT = 'https://www.huawei.com/health/health.wgt';
    const HEALTH_WGT_READONLY = 'https://www.huawei.com/health/health.wgt.readonly';
    const HEALTH_SLP = 'https://www.huawei.com/health/health.slp';
    const HEALTH_SLP_READONLY = 'https://www.huawei.com/health/health.slp.readonly';
    const HEALTH_HR = 'https://www.huawei.com/health/health.hr';
    const HEALTH_HR_READONLY = 'https://www.huawei.com/health/health.hr.readonly';
    const HEALTH_ECG = 'https://www.huawei.com/health/health.ecg';
    const HEALTH_ECG_READONLY = 'https://www.huawei.com/health/health.ecg.readonly';
    const HEALTH_BG = 'https://www.huawei.com/health/health.bg';
    const HEALTH_BG_READONLY = 'https://www.huawei.com/health/health.bg.readonly';
    const MOTIONPATH = 'https://www.huawei.com/health/motionpath';
    const MOTIONPATH_READONLY = 'https://www.huawei.com/health/motionpath.readonly';
    const ACTIVITY = 'https://www.huawei.com/health/activity';
    const NOTIFY = 'https://www.huawei.com/health/notify';
    const PUSH = 'https://www.huawei.com/health/push';
    const HEALTH_BP = 'https://www.huawei.com/health/health.bp';
    const HEALTH_BP_READONLY = 'https://www.huawei.com/health/health.bp.readonly';
    const HEALTH_PS_READONLY = 'https://www.huawei.com/health/health.ps.readonly';
    const HEALTH_RT_READONLY = 'https://www.huawei.com/health/health.rt.readonly';
    const TRANSCRIPT_READONLY = 'https://www.huawei.com/health/transcript.readonly';
    const TRANSCRIPT = 'https://www.huawei.com/health/transcript';
    const HUAWEIID = 'https://www.huawei.com/health/huaweiid';
    const HUAWEIID_READONLY = 'https://www.huawei.com/health/huaweiid.readonly';
    const DEVICE = 'https://www.huawei.com/health/device';
    const DEVICE_READONLY = 'https://www.huawei.com/health/device.readonly';

    // health kit
    // 个人数据
    const HEIGHT_WEIGHT_READ = 'https://www.huawei.com/healthkit/heightweight.read'; // 查看华为运动健康服务中的身高和体重
    const HEIGHT_WEIGHT_WRITE = 'https://www.huawei.com/healthkit/heightweight.write'; // 存储身高和体重到华为运动健康服务
    
    // 健康数据
    const SLEEP_READ = 'https://www.huawei.com/healthkit/sleep.read'; // 查看华为运动健康服务中的睡眠数据
    const SLEEP_WRITE = 'https://www.huawei.com/healthkit/sleep.write'; // 存储睡眠数据到华为运动健康服务中

    const HEARTRATE_READ = 'https://www.huawei.com/healthkit/heartrate.read'; // 查看华为运动健康服务中的心率数据（包括心率、静息心率、运动心率）
    const HEARTRATE_WRITE = 'https://www.huawei.com/healthkit/heartrate.write'; // 存储心率数据到华为运动健康服务（包括心率、静息心率、运动心率）

    const NUTRITION_READ = 'https://www.huawei.com/healthkit/nutrition.read'; // 查看华为运动健康服务中的营养数据
    const NUTRITION_WRITE = 'https://www.huawei.com/healthkit/nutrition.write'; // 存储营养数据到华为运动健康服务

    const STRESS_READ = 'https://www.huawei.com/healthkit/stress.read'; // 查看华为运动健康服务中的压力数据
    const STRESS_WRITE = 'https://www.huawei.com/healthkit/stress.write'; // 存储压力数据到华为运动健康服务

    // 运动数据
    const STEP_READ= 'https://www.huawei.com/healthkit/step.read'; // 查看华为运动健康服务中的步数
    const STEP_WRITE = 'https://www.huawei.com/healthkit/step.write'; // 存储步数到华为运动健康服务
    const DISTANCE_READ = 'https://www.huawei.com/healthkit/distance.read'; // 查看华为运动健康服务中的距离和爬高
    const DISTANCE_WRITE = 'https://www.huawei.com/healthkit/distance.write'; // 存储距离和爬高到华为运动健康服务
    const SPEED_READ = 'https://www.huawei.com/healthkit/speed.read'; // 查看华为运动健康服务中的速度数据
    const SPEED_WRITE = 'https://www.huawei.com/healthkit/speed.write'; // 存储速度到华为运动健康服务
    const CALORIES_READ = 'https://www.huawei.com/healthkit/calories.read'; // 查看华为运动健康服务中的热量（包括基础代谢）
    const CALORIES_WRITE = 'https://www.huawei.com/healthkit/calories.write'; // 存储热量（包括基础代谢）到华为运动健康服务
    const STRENGTH_READ = 'https://www.huawei.com/healthkit/strength.read'; // 查看华为运动健康服务中的中高强度数据
    const STRENGTH_WRITE = 'https://www.huawei.com/healthkit/strength.write'; // 存储中高强度数据到华为运动健康服务
    const ACTIVITY_READ = 'https://www.huawei.com/healthkit/activity.read'; // 查看华为运动健康服务中的活动数据（如活动打点、锻炼、力量、跑步姿势、骑行等）
    const ACTIVITY_WRITE = 'https://www.huawei.com/healthkit/activity.write'; // 存储活动数据（如活动打点、锻炼、力量、跑步姿势、骑行等）到华为运动健康服务
    const LOCATION_READ = 'https://www.huawei.com/healthkit/location.read'; // 查看华为运动健康服务中的位置数据（包括轨迹）
    const LOCATION_WRITE = 'https://www.huawei.com/healthkit/location.write'; // 存储位置数据（包括轨迹）到华为运动健康服务

    // 受限数据
    const BLOODGLUCOSE_READ = 'https://www.huawei.com/healthkit/bloodglucose.read'; // 查看华为运动健康服务中的血糖数据
    const BLOODGLUCOSE_WRITE = 'https://www.huawei.com/healthkit/bloodglucose.write'; // 存储血糖数据到华为运动健康服务
    const BLOODPRESSURE_READ = 'https://www.huawei.com/healthkit/bloodpressure.read'; // 查看华为运动健康服务中的血压数据
    const BLOODPRESSURE_WRITE = 'https://www.huawei.com/healthkit/bloodpressure.write'; // 存储血压数据到华为运动健康服务
    const OXYGENSATURATION_READ = 'https://www.huawei.com/healthkit/oxygensaturation.read'; // 查看华为运动健康服务中的血氧数据
    const OXYGENSATURATION_WRITE = 'https://www.huawei.com/healthkit/oxygensaturation.write'; // 存储血氧数据到华为运动健康服务
    const BODYTEMPERATURE_READ = 'https://www.huawei.com/healthkit/bodytemperature.read'; // 查看华为运动健康服务中的体温数据
    const BODYTEMPERATURE_WRITE = 'https://www.huawei.com/healthkit/bodytemperature.write'; // 存储体温数据到华为运动健康服务
    const REPRODUCTIVE_READ = 'https://www.huawei.com/healthkit/reproductive.read'; // 查看华为运动健康服务中的生殖数据
    const REPRODUCTIVE_WRITE = 'https://www.huawei.com/healthkit/reproductive.write'; // 存储生殖数据到华为运动健康服务
    const HEARTHEALTH_READ = 'https://www.huawei.com/healthkit/hearthealth.read'; // 查看华为运动健康服务中心脏健康相关数据
    const HEARTHEALTH_WRITE = 'https://www.huawei.com/healthkit/hearthealth.write'; // 存储心脏健康相关数据到华为运动健康服务

    // 运动数据
    const ACTIVITYRECORD_READ = 'https://www.huawei.com/healthkit/activityrecord.read'; // 查看华为运动健康服务中的用户运动记录
    const ACTIVITYRECORD_WRITE = 'https://www.huawei.com/healthkit/activityrecord.write'; // 存储用户运动记录到华为运动健康服务

    // 历史数据
    const HISTORYDATA_OPEN = 'https://www.huawei.com/healthkit/historydata.open'; // 读取授权之前的历史数据
}